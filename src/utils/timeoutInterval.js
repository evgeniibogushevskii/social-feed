const timeoutInterval = interval => {
  if (interval < 1 || interval > 1000) {
    return 1000;
  }
  return interval * 1000;
};

export { timeoutInterval };
