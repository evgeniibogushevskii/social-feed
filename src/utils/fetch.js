import axios from 'axios';

const fetch = async url => {
  try {
    const request = await axios.get(url);
    return request.data;
  } catch (err) {
    return [];
  }
};

export { fetch };
