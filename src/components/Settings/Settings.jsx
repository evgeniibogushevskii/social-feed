import React, { Component } from 'react';
import './Settings.css';

class Settings extends Component {
  state = {
    feedURL: this.props.appliedSettings.feedURL,
    interval: this.props.appliedSettings.interval,
    postsNumber: this.props.appliedSettings.postsNumber
  };

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  settingsSubmit = e => {
    e.preventDefault();
    const { feedURL, interval, postsNumber } = this.state;
    this.props.setSettings(feedURL, interval, postsNumber);
  };

  render() {
    return (
      <form
        style={{ visibility: this.props.isOpen ? 'visible' : 'hidden' }}
        className="settingsForm"
      >
        <div className="field">
          <label htmlFor="feedURL" className="labelFeed">
            Feed URL:
          </label>
          <input
            className="input"
            type="text"
            name="feedURL"
            id="feedURL"
            value={this.state.feedURL}
            onChange={this.handleChange}
          />
        </div>
        <div className="field">
          <label htmlFor="interval" className="labelInterval">
            Interval (in seconds):
          </label>
          <input
            className="input"
            type="number"
            name="interval"
            id="interval"
            value={this.state.interval}
            onChange={this.handleChange}
            min="1"
          />
        </div>
        <div className="field">
          <label htmlFor="postsNumber" className="labelPosts">
            Number of posts:
          </label>
          <input
            className="input"
            type="number"
            name="postsNumber"
            id="postsNumber"
            value={this.state.postsNumber}
            onChange={this.handleChange}
            min="1"
          />
        </div>
        <button type="button" onClick={this.settingsSubmit}>
          OK
        </button>
      </form>
    );
  }
}

export { Settings };
