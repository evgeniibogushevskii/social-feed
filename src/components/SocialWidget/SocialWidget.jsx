import React, { Component } from 'react';
import { Settings } from '../Settings';
import { Feed } from '../Feed';
import { fetch, timeoutInterval } from '../../utils';
import './SocialWidget.css';

class SocialWidget extends Component {
  state = {
    feed: [],
    feedURL: 'https://my-json-server.typicode.com/jackfixard/feed/posts',
    interval: 60,
    postsNumber: 5,
    settingsOpen: false
  };

  componentDidMount() {
    this.refreshData();
  }

  async refreshData() {
    console.log('Refreshed!');
    const { feedURL, interval } = this.state;
    const feed = await fetch(feedURL);
    this.setState({ feed });
    setTimeout(this.refreshData.bind(this), timeoutInterval(interval));
  }

  openSettings = e => {
    e.preventDefault();
    this.setState({ settingsOpen: true });
  };

  handleSettings = (feedURL, interval, postsNumber) => {
    this.setState(
      {
        feedURL,
        interval,
        postsNumber,
        settingsOpen: false
      },
      () => {
        this.refreshData();
      }
    );
  };

  render() {
    const appliedSettings = {
      feedURL: this.state.feedURL,
      interval: this.state.interval,
      postsNumber: this.state.postsNumber
    };
    return (
      <div className="widget">
        <Feed posts={this.state.feed} quantity={this.state.postsNumber} />
        <div className="settings">
          <button onClick={this.openSettings}>></button>
          <Settings
            setSettings={this.handleSettings}
            isOpen={this.state.settingsOpen}
            appliedSettings={appliedSettings}
          />
        </div>
      </div>
    );
  }
}

export { SocialWidget };
