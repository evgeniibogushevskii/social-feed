import React from 'react';
import { SocialWidget } from '../SocialWidget';
import './App.css';

const App = () => {
  return (
    <div className="app">
      <SocialWidget />
    </div>
  );
};

export { App };
