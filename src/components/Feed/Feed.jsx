import React from 'react';
import { Post } from '../Post';
import './Feed.css';

const Feed = ({ posts, quantity }) => {
  if (!Array.isArray(posts) || posts.length === 0 || quantity < 1) {
    return <div className="feed">No posts available</div>;
  }
  if (quantity > 100000) {
    return <div className="feed">Too much...</div>;
  }
  const postList = posts
    .slice(0, quantity)
    .map(post => <Post key={post.id} post={post} />);

  return <div className="feed">{postList}</div>;
};

export { Feed };
