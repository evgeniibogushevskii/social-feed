import React from 'react';
import './Post.css';

const Post = ({ post }) => {
  return (
    <div className="post">
      <div className="date">{post.date}</div>
      <div className="author">{post.author}</div>
      <div className="message">{post.message}</div>
    </div>
  );
};

export { Post };
