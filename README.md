# Hello!

**Welcome to FEED**

To start this project simply execute this two commands in your console:

- `yarn`
- `yarn start`

After that you'll be able to see project on http://localhost:3000/

_In my opinion this looks nice but I have some questions and a few thoughts about how to improve this nice little project_

## For example

- Use TypeScript
- Maybe I should use Redux, Mobx, React Hooks? Because now it's written without using all of these
- FEED is a small project without future expansion but maybe it's a good idea to write some tests
- I need to know in what format FEED receives date and time in order to parse it right and show it in the right format
- Also would be good to know about desired values of update interval so I can either add some more validations on input field or even change the input type
- For now I just take the first N values of the received array as a latest posts. But maybe the latest posts would be in the end of an array, so it would be good to know where exactly latest posts placed (of course we can sort received array by date in objects, but this is a lot of an additional computations due to high frequency of updating and the lack of limitation on size of receiving data)
- Also for now I mocked the static data, but FEED pull up the data with interval from settings and shows it correctly. Trust me, I checked this :)
- Design of FEED may look a bit too minimalist but I just wanted to focus on code first. Design improvements will come with the next patch!

Thanks for reading, and have a nice day :)
